# Coeus ~> Redis-HA

Custom Helm Charts to install a Highly available Redis database service.
This chart is based on the [official chart](https://github.com/helm/charts/tree/master/stable/redis-ha)

## How to

The namespace must be define in the variable **GIVEN_NAMESPACE**, before launching the pipeline.

## Network Policy

The [default policy](https://gitlab.com/bodzen/continuous-deployments/redis-ha/blob/master/policy/redis-policy.yaml) is to deny all **INGRESS** and **EGRESS** traffic.

To communicate with the redis cluster, you must create a policy specific the application deployed.

### Example

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-new-app-redis-policy
  namespace: GIVEN_NAMESPACE
spec:
  podSelector:
    matchLabels:
      app: redis-ha
  ingress:
    - from:
	  - podSelector:
	    matchLabels:
		  app: "my-new-app"
  policyTypes:
    - Ingress
```


## Helm installation output

```
NAME:   coeus-redis-00
LAST DEPLOYED: Fri Nov  1 13:24:52 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/ConfigMap
NAME                               DATA  AGE
coeus-redis-00-redis-ha-configmap  4     2s

==> v1/Pod(related)
NAME                              READY  STATUS   RESTARTS  AGE
coeus-redis-00-redis-ha-server-0  0/2    Pending  0         1s

==> v1/Role
NAME                     AGE
coeus-redis-00-redis-ha  2s

==> v1/RoleBinding
NAME                     AGE
coeus-redis-00-redis-ha  2s

==> v1/Service
NAME                                TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)             AGE
coeus-redis-00-redis-ha             ClusterIP  None            <none>       6379/TCP,26379/TCP  1s
coeus-redis-00-redis-ha-announce-0  ClusterIP  10.245.91.141   <none>       6379/TCP,26379/TCP  2s
coeus-redis-00-redis-ha-announce-1  ClusterIP  10.245.219.149  <none>       6379/TCP,26379/TCP  1s
coeus-redis-00-redis-ha-announce-2  ClusterIP  10.245.92.88    <none>       6379/TCP,26379/TCP  1s

==> v1/ServiceAccount
NAME                     SECRETS  AGE
coeus-redis-00-redis-ha  1        2s

==> v1/StatefulSet
NAME                            READY  AGE
coeus-redis-00-redis-ha-server  0/3    1s


NOTES:
Redis can be accessed via port 6379 and Sentinel can be accessed via port 26379 on the following DNS name from within your cluster:
coeus-redis-00-redis-ha.default.svc.cluster.local

To connect to your Redis server:
1. Run a Redis pod that you can use as a client:

   kubectl exec -it coeus-redis-00-redis-ha-server-0 sh -n default

2. Connect using the Redis CLI:

  redis-cli -h coeus-redis-00-redis-ha.default.svc.cluster.local

```
