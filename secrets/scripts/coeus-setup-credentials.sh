#!/bin/bash
# Create secret used by the Helm chart to setup Redis-ha

set -e
source $OUTPUT_FUNC

export SECRET_NAME="coeus-redis-ha"
MANIFEST_FILE="${MANIFEST_PATH}/coeus-setup-credentials.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	sed -i 's/REDIS_TOKEN/'"$REDIS_TOKEN_B64"'/g' $MANIFEST_FILE
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
