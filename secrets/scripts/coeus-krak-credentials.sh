#!/bin/bash
# Create secret containing a Redis URI

set -e
source $OUTPUT_FUNC

export SECRET_NAME="coeus-redis-credentials"
MANIFEST_FILE="${MANIFEST_PATH}/coeus-krak-credentials.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	REDIS_BASE_URL="redis://_:${REDIS_TOKEN}@coeus-redis-ha.${GIVEN_NAMESPACE}.svc.cluster.local"
	REDIS_URL_B64=$(base64 <<< "$REDIS_BASE_URL" | tr -d '\n')
	sed -i 's/REDIS_CREDENTIALS/'"$REDIS_URL_B64"'/g' $MANIFEST_FILE
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
